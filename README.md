### Relayr interview competition

**First task** is in the `reflections.md` file.

**Second task** completed in `test_cases.md` file.

**Third task:**

This is an experiment with a new framework to me, Selene.

To run:

1. Pick your fav virtual env and activate it

1. `pip install git+https://github.com/yashaka/selene.git`

1. `pip install faker`

1. `pytest relayr/tests/test_search_features.py`

Easter egg: http://duckduckgo.com/?q=why+should+i+use+this