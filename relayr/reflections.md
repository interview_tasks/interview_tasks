# Test
#### 1. Imagine the following situation. 
#### You need to establish a QA process in a cross-functional team. 
#### The team builds a front-end application using REST APIs.

##### 1. Where would you start? What would be your first steps?

Most of the projects are underestimating the role and the value of the QA.
Thus the step one will take proving the necessity of quality and describing the 
term of the health of the project.

Then I will have to be sure that the project's requirements and documentation 
are correctly formulated.

Not last, and not least, the logging should be helpful and informative.
Sometimes it's required to correct or even implement the proper documentation of
the functionalities and the code itself.

Knowing that the whole process of the Quality Assurance has had to be started at
the beginning of the project, along with the idea, the existing functionalities 
have to be immediately divided by the significance to the buisiness, marked with
priorities and covered with the regression tests.

Regression tests are written according the use cases and user stories and 

collected to scenarios by priority.

The most important scenarios become smoke/sanity checklists. 

At every stage of development process it's crucial to be sure that the code is 
thoroughly tested.

##### 2. Which process would you establish around testing new functionality? 
##### How would you want the features to be tested?

**The process:**
Any new feature has to be tested firstly by its developer, not too take much 
time, but it must work and satisfy the story at least.

Then, using the issue tracker it should be delegated to the QA department. 
The manual testing happens here, starting from the description and comments 
in the tool.

When the testing engineer is satisfied, he launches the critical and all 
necessary tests in CI and only after the green light merges the feature branch 
to the release trunk (not production yet).

When something goes wrong it's up to QA what to do - reopen (or create a 
dependent issue) the task or just tell the developer to fix something fast.

After, or during the test, QA should write or change the existing test-cases 
for them to be automated.

**Documentation and liasing:**
New features should be well-documented. Every piece of documentation should be 
assessed as it would be code.

Conducting story workshop sessions is a good practice for everyone to understand
new features in details. Probably, after each release, the retrospectives should 
take part.

**Automation:**
Obviously the whole new code should be well unit tested by programmers. 
System and integration tests are to be written by coders or testers, API and UI 
tests are for the latter.

Every piece of code should be reviewed by the colleague.

Automated tests should be grouped and assigned to CI for it to launch them by 
schedule and on some events (i.e. preventing merge before every test passes).


##### 3. Which tools would you suggest using to help your team with a daily work?

- Common sense
- Browser Dev Tools (Chrome, Firefox)
- Documentation repository (Confluence, Git)
- Test management (TestRail, TestLink)
- Issue tracking (JIRA, Bugzilla, Redmine)
- VCS (Git)
- CI (Jenkins, GitLab CI, )
- Logs (tail -f, grep, ctrl+f, )
- Chat (Mattermost, Slack, Gitter, )
- IDE (IDEA)
- Load testing tool (JMeter, Yandex Tank, Loadrunner)
- Test Automation (Selenium, pyTest, headless browsers, Docker, Postman, SoapUI)
- Mobile testing tool (Charles proxy, Appium, )

##### 4. If you would do a test automation which techniques or best practices 
##### would you use the application?

If it comes to UI testing it's the common best practice to use the PageObject 
pattern, it's handy in code comprehension and support. The tests come really 
flexible, consize and resilent.
I'd also suggest using obvious programming principles like DRY and Fail Fast.
For the UI tests the most important guidances are: 
- The tests should not be brittle: the locators must be resilent and waits should 
use the nature of the interface, not things like `sleep(1)`.
- One test, one assertion.

And the set of recomendations to any type of test:
- Fast: as fast as possible.
- Minimum of hardcode: most of the tests should be parametrized to be able to be 
reused.
- Readable, documented: for anyone can uderstand and support them.
- Repeatable: run repeatedly in any order.
- Self-validating: no manual evaluation required.