from selene import config
from selene.browser import open_url
from selene.bys import *
from selene.support.jquery_style_selectors import s

from relayr.pages.search_results_page import SearchResultsPage

config.browser_name = 'chrome'


class SearchPage(object):
    search_feild = '//*[@id="search_form_input_homepage"]'
    search_btn = '//*[@id="search_button_homepage"]'
    options_bar = '//*[@id="duckbar"]'

    web_search = options_bar + '//[@data-zci-link="web"]'
    images_search = options_bar + '//[@data-zci-link="images"]'
    videos_search = options_bar + '//[@data-zci-link="videos"]'

    def open(self):
        open_url("http://duckduckgo.com")
        return self

    def search(self, text):
        s(by_xpath(self.search_feild)).set(text).press_enter()
        return SearchResultsPage()
