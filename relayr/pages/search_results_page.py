from selene.support.jquery_style_selectors import s, ss
from selene.bys import *


class SearchResultsPage(object):
    AWESOME_TEXT = "cause it's awesome"

    search_field = '//*[@id="search_form"]'
    results_div = '//*[@id="links"]'
    easter_egg_div = '//*[@id="zero_click_wrapper"]'

    safe_search_ddl = '.dropdown.dropdown--safe-search'

    def __init__(self):
        self.results = ss(by_xpath(self.results_div))

    def search_why(self):
        return s(by_xpath(self.easter_egg_div))

    def switch_safe_search_off(self):
        s(self.safe_search_ddl).click()
        s(by_text('Off')).click()
