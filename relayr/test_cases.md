## 1 Search values:
##### 1.1 Does the Images search work?

*Prerequisites*

1.1.1 open https://duckduckgo.com

*Steps*
1.1.2 enter the search text

1.1.3 switch to "Images"

*Result*

1.1.4 the search result should contain images


##### 1.2 Does the Videos search work?

*Prerequisites*

1.2.1 open https://duckduckgo.com

*Steps*

1.2.2 enter the search text

1.2.3 switch to "Videos"

*Result*

1.2.4 the search result should contain videos


##### 1.3 Does the News search work?
*Prerequisites*

1.3.1 open https://duckduckgo.com

*Steps*

1.3.2 enter the relevant search text

1.3.3 switch to "News"

*Result*

1.3.4 results should contain News resources


##### 1.4 Search is not case sensitive.

*Prerequisites*

1.4.1 open https://duckduckgo.com

*Steps*

1.4.2 search for some text

1.4.3 search the same value with capitalization

*Result*

1.4.4 search results do not differ


##### 1.5 Does the search work for special characters?

*Prerequisites*

1.5.1 open https://duckduckgo.com

*Steps*

1.5.2 search for special characters {[(~!@#$%^&`|\:”;’<>?,./*-+)]}

*Result*

1.5.3 search results are about the characters


##### 1.6 "Enter" and "Search" button.

*Prerequisites*

1.6.1 open https://duckduckgo.com

*Steps*

1.6.2 enter some value in the search box

1.6.3 press Enter key

1.6.4 press Search button

*Result*

1.6.5 search fetches the results in both cases

##### 1.7 Does the "Clear Form" button work?

*Prerequisites*

1.7.1 open https://duckduckgo.com

*Steps*

1.7.2 enter some value in the search box

1.7.3 press the "x" button

*Result*

1.7.3 search box becomes empty


##### 1.8 Advanced search.

*Prerequisites*

1.7.1 open https://duckduckgo.com

*Steps*

1.7.2 enter some value in the search box

1.7.3 choose the country

*Result*

1.7.3 check the results are relevant to the country and the sources are that 

country websites


## 2 Search results:


##### 2.1 Phonetic searching.

*Prerequisites*

2.1.1 open https://duckduckgo.com

*Steps*

2.1.2 type a word with typo
*Result*

2.1.3 results should contain correctly spelled word, without a typo

2.1.4 under the search box, there should be two lines informing that results 
include the word spelled correctly and the link to search strictly for the 
entered text

##### 2.2 Back - forward navigation.

*Prerequisites*

2.2.1 open https://duckduckgo.com

*Steps*

2.2.2 perform some different searches

2.2.3 navigate Back and Forward

*Result*

2.2.4 the page should not expire


##### 2.3 Do the search results look as expected?

*Prerequisites*

2.3.1 open https://duckduckgo.com

*Steps*

2.3.2 search something

*Result*

2.3.4 each search result should contain a link and a few lines containing the 
searched keywords and the link should navigate to the page where the keywords 
exist
2.3.5 searched keyword should be highlighted in the search result page and also
in the page where the keyword exists


##### 2.4 Search indexing:

*Prerequisites*

2.4.1 open https://duckduckgo.com

*Steps*

2.4.3 navigate to News

*Result*

2.4.5 the results should contain fresh news


## 3 Search security:


##### 3.1 Strict safety.

*Prerequisites*

3.1.1 open https://duckduckgo.com

3.1.2 Safe Search: Strict

*Steps*

3.1.3 search something adult (i.e."porn")

*Result*

3.1.5 There should be no results

3.1.6 The "Safe search blocked results for porn." text should appear.


##### 3.2 No safety.

*Prerequisites*

3.2.1 open https://duckduckgo.com

3.2.2 Safe Search: Off

*Steps*

3.2.3 search something adult (i.e."porn")

*Result*

3.2.5 There should be results
