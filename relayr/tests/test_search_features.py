from selene.support.conditions import have
from relayr.pages.search_page import SearchPage, SearchResultsPage
from faker import Faker

results_page = SearchResultsPage()


def test_duck_why():
    """
    Test the built in easter egg.
    """
    duck = SearchPage().open()
    text = SearchResultsPage().AWESOME_TEXT

    duck.search("why should i use this")
    egg = results_page.search_why()

    assert egg.should(have.text(text))


def test_duck_word():
    """
    Assure it can find any valid word.
    """
    duck = SearchPage().open()
    faker = Faker()
    request = faker.word()

    duck.search(request)
    results = results_page.results[0]

    assert results.should(have.text(request))


def test_every_of_three():
    """
    Assure every word of 3 random words will be presented.
    """
    duck = SearchPage().open()
    faker = Faker()

    random_words = faker.words(3)
    request = ' '.join(random_words)

    duck.search(request)
    results = results_page.results[0]

    for i in request:
        assert i in results.text


def test_adult():
    """
    Check the functionality of the Safe Search feature.
    """
    duck = SearchPage().open()
    adult_word = 'porn'

    duck.search(adult_word)
    results = results_page.results[0]
    print(results_page.results)

    results_page.switch_safe_search_off()

    assert results.should(have.text(adult_word))
